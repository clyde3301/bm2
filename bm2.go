// Author: clyde3301
// Fri, 27 Oct 2023 03:30:15 +0200
//
// to do:
// - fix PascalCase
// - add 'edit' option
//

package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
)

var (
	linesPerPage = 10
	listLocation string
)

func List(arg2 int, linesPerPage int) {
	Page := 1

	if arg2 != 0 {
		Page = arg2
	}

	File, err := os.Open(listLocation)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file:", err)
		os.Exit(1)
	}
	defer File.Close()

	Scanner := bufio.NewScanner(File)
	TotalLines := 0
	for Scanner.Scan() {
		TotalLines++
	}

	NumPages := (TotalLines + linesPerPage - 1) / linesPerPage

	if Page < 1 || Page > NumPages {
		fmt.Fprintf(os.Stderr, "Invalid page number. Page number should be between 1 and %d.\n", NumPages)
		os.Exit(1)
	}

	StartLine := (Page - 1) * linesPerPage
	EndLine := StartLine + linesPerPage

	fmt.Printf("Number of Pages: %d\n", NumPages)
	fmt.Printf("Page %d:\n", Page)

	File.Seek(0, 0)
	Scanner = bufio.NewScanner(File)
	LineNum := 1
	for Scanner.Scan() {
		if LineNum > StartLine && LineNum <= EndLine {
			fmt.Printf("%d: %s\n", LineNum, Scanner.Text())
		}
		LineNum++
	}
}

func Add() {
	fmt.Print("Enter content: ")
	Scanner := bufio.NewScanner(os.Stdin)
	Scanner.Scan()
	Content := Scanner.Text()

	File, err := os.OpenFile(listLocation, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file:", err)
		os.Exit(1)
	}
	defer File.Close()

	_, err = fmt.Fprintln(File, Content)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error writing to file:", err)
		os.Exit(1)
	}
}

func DeleteLines(Start, End int) {
	File, err := os.OpenFile(listLocation, os.O_RDWR, 0644)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file:", err)
		os.Exit(1)
	}
	defer File.Close()

	Scanner := bufio.NewScanner(File)
	var NewLines []string
	Line := 1
	for Scanner.Scan() {
		if Line < Start || Line > End {
			NewLines = append(NewLines, Scanner.Text())
		}
		Line++
	}

	File.Truncate(0)
	File.Seek(0, 0)
	for _, Line := range NewLines {
		_, err = fmt.Fprintln(File, Line)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error writing to file:", err)
			os.Exit(1)
		}
	}
}

func DeleteLine(LineNum int) {
	File, err := os.OpenFile(listLocation, os.O_RDWR, 0644)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file:", err)
		os.Exit(1)
	}
	defer File.Close()

	Scanner := bufio.NewScanner(File)
	var NewLines []string
	CurrentLine := 1
	for Scanner.Scan() {
		if CurrentLine != LineNum {
			NewLines = append(NewLines, Scanner.Text())
		}
		CurrentLine++
	}

	File.Truncate(0)
	File.Seek(0, 0)
	for _, Line := range NewLines {
		_, err = fmt.Fprintln(File, Line)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error writing to file:", err)
			os.Exit(1)
		}
	}
}

func main() {
	usr, err := user.Current()
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	configDir := filepath.Join(usr.HomeDir, ".config")
	if _, err := os.Stat(configDir); os.IsNotExist(err) {
		err := os.Mkdir(configDir, 0755)
		if err != nil {
			fmt.Println("Error creating config directory:", err)
			return
		}
	}

	configFile := filepath.Join(configDir, "bm2.config")

	if _, err := os.Stat(configFile); os.IsNotExist(err) {
		defaultConfig := fmt.Sprintf("linesPerPage: 10\nlistLocation: %s\n", usr.HomeDir)
		err := ioutil.WriteFile(configFile, []byte(defaultConfig), 0644)
		if err != nil {
			fmt.Println("Error creating config file:", err)
			return
		}
	}

	configData, err := ioutil.ReadFile(configFile)
	if err != nil {
		fmt.Println("Error reading config file:", err)
		return
	}

	configText := string(configData)
	lines := strings.Split(configText, "\n")

	config := make(map[string]string)
	for _, line := range lines {
		parts := strings.Split(line, ":")
		if len(parts) == 2 {
			key := strings.TrimSpace(parts[0])
			value := strings.TrimSpace(parts[1])
			config[key] = value
		}
	}

	listLocationPath := filepath.Join(usr.HomeDir, "list")
	if _, err := os.Stat(listLocationPath); os.IsNotExist(err) {
		err := ioutil.WriteFile(listLocationPath, []byte("\n"), 0644)
		if err != nil {
			fmt.Println("Error creating listLocation file:", err)
			return
		}
	}
	linesPerPageStr := config["linesPerPage"]
	linesPerPage, err := strconv.Atoi(linesPerPageStr)

	if err != nil || linesPerPage <= 0 {
		fmt.Fprintf(os.Stderr, "Invalid linesPerPage value: %s\n", linesPerPageStr)
		os.Exit(1)
	}
	listLocation = config["listLocation"]

	//debug
	//fmt.Println("linesPerPage:", linesPerPage)
	//fmt.Println("listLocation:", listLocation)
	//

	if len(os.Args) < 2 {
		fmt.Fprintln(os.Stderr, "Usage: bm [list | add | delete]")
		os.Exit(1)
	}

	Command := os.Args[1]

	switch Command {
	case "list":
		Arg2 := 0
		if len(os.Args) > 2 {
			Arg2, _ = strconv.Atoi(os.Args[2])
		}
		List(Arg2, linesPerPage)

	case "add":
		Add()

	case "delete":
		if len(os.Args) == 3 {
			LineNum, err := strconv.Atoi(os.Args[2])
			if err != nil {
				fmt.Fprintln(os.Stderr, "Invalid line number:", err)
				os.Exit(1)
			}
			DeleteLine(LineNum)
		} else if len(os.Args) == 4 {
			Start, err1 := strconv.Atoi(os.Args[2])
			End, err2 := strconv.Atoi(os.Args[3])
			if err1 != nil || err2 != nil {
				fmt.Fprintln(os.Stderr, "Invalid line numbers:", err1, err2)
				os.Exit(1)
			}
			DeleteLines(Start, End)
		} else {
			fmt.Fprintln(os.Stderr, "Usage: bm delete [line_number | start end]")
			os.Exit(1)
		}
	}
}
